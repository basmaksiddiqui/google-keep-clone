import { Component, OnInit } from '@angular/core';
import { Note } from '../models/note.model';
import { NoteService } from '../note.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  notesList : Array<Note>;
  newNoteForm : FormGroup;
  newNote : Note;
  constructor(private noteService : NoteService,
              private formBuilder : FormBuilder) {
                this.newNoteForm = this.formBuilder.group({
                  title:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                  description:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                  createdBy:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                  createdOn:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                  category:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                  priority:['',Validators.compose([Validators.required,Validators.minLength(5)])],
                }) 
   
  }

  ngOnInit(): void {
     this.noteService.getNotes().subscribe(res =>{
       console.log('Res', res);
       this.notesList = res;
     });
  }

  addNewNote(newNote : FormGroup){
    this.newNoteForm = newNote.value;
  }

}
