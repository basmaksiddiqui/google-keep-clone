import { Injectable } from '@angular/core';
import { Note } from './models/note.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  notesList : Array<Note>;

  NOTES_END_POINT_URL : string;

  constructor(private httpClient : HttpClient) {
    this.NOTES_END_POINT_URL = "http://localhost:3000/notes";
   }

   getNotes() : Observable<Array<Note>>{
     return this.httpClient.get<Array<Note>>(this.NOTES_END_POINT_URL);
   }
}
