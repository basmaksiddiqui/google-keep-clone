import { Component, OnInit } from '@angular/core';
import { SignupUser } from './signupuser.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupUser : SignupUser;
  signupForm : NgForm;
  constructor() {
    this.signupUser = new SignupUser();
  }

  ngOnInit(): void {
  }

  userSignup(sform : NgForm){
    
    this.signupUser = sform.value;
    console.log(this.signupUser);
    
  }

}